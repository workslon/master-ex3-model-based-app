var AppDispatcher = require('../dispatchers/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var CHANGE_EVENT = 'change';

var books = [];
var notifications = {
  status: 'idle',
  errors: {}
};

var AppStore = Object.assign({}, EventEmitter.prototype, {
  getBook: function getBook(id) {
    return books.filter(function (book) {
      return book.objectId === id;
    })[0];
  },

  getAllBooks: function () {
    return books;
  },

  getNotifications: function () {
    return notifications;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Get all books
    case 'REQUEST_BOOKS_SUCCESS':
      try {
        books = action.result;
        AppStore.emitChange();
      } catch (e) {
        alert('Unvalid remote response format!');
      }
      break;

    // -- Create book Pending
    case 'REQUEST_BOOK_SAVE':
      notifications.status = 'pending';
      AppStore.emitChange();
      break;

    // -- Create book Success
    case 'BOOK_SAVE_SUCCESS':
      try {
        action.data.objectId = JSON.parse(action.result).objectId;
        books.push(action.data);
        notifications.errors = {};
        notifications.status = 'success';
        AppStore.emitChange();
      } catch (e) {}
      break;

    // -- Create book Error
    case 'BOOK_SAVE_ERROR':
      notifications.status = 'error';
      notifications.errors = action.error;
      AppStore.emitChange();
      break;

    // -- Update book Pending
    case 'REQUEST_BOOK_UPDATE':
      notifications.status = 'pending';
      AppStore.emitChange();
      break;

    // --- Update book Success
    case 'BOOK_UPDATE_SUCCESS':
      books = books.map(function (book) {
        if (book.objectId === action.data.objectId) {
          book.title = action.data.title;
          book.year = action.data.year;
        }
        return book;
      });
      notifications.errors = {};
      notifications.status = 'success';
      AppStore.emitChange();
      break;

    // -- Update book Error
    case 'BOOK_UPDATE_ERROR':
      notifications.status = 'error';
      notifications.errors = action.error;
      AppStore.emitChange();
      break;

    // -- Destroy book Pending
    case 'REQUEST_BOOK_DESTROY':
      notifications.status = 'pending';
      AppStore.emitChange();
      break;

    // -- Destroy book Success
    case 'BOOK_DESTROY_SUCCESS':
      books = books.filter(function (book) {
        return book.objectId !== action.data.objectId;
      });
      notifications.status = 'idle';
      AppStore.emitChange();
      break;

    // --- Client Validation error
    case 'BOOK_VALIDATION_ERROR':
      Object.keys(action.errors).map(function (key) {
        notifications.errors[key] = action.errors[key];
      });

      notifications.status = 'error';
      AppStore.emitChange();
      break;

    // --- Non-unique ISBN
    case 'NON_UNIQUE_ISBN':
      notifications.status = 'error';
      notifications.errors = {
        isbn: 'The book with such ISBN already exists!'
      };
      AppStore.emitChange();
      break;

    // --- Clear all notifications (eather errors or success)
    case 'CLEAR_NOTIFICATIONS':
      notifications = {
        status: 'idle',
        errors: {}
      };
      AppStore.emitChange();
      break;
  }
});

module.exports = AppStore;