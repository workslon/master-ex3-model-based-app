var AppDispatcher = require('../dispatchers/AppDispatcher');
var adapter = require('../storage-managers/ParseApiAdapter');

module.exports = {
  validate: function(modelClass, key, value) {
    var errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: 'BOOK_VALIDATION_ERROR',
      errors: errors
    });
  },

  getBooks: function (modelClass) {
    var promise = adapter.retrieveAll(modelClass);

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOKS',
      success: 'REQUEST_BOOKS_SUCCESS',
      failure: 'REQUEST_BOOKS_ERROR'
    });
  },

  createBook: function (modelClass, data) {
    adapter
      .retrieve(modelClass, '', data.isbn)
      .then(function(records) {
        try {
          if (records.length) {
            AppDispatcher.dispatch({
              type: 'NON_UNIQUE_ISBN'
            });
          } else {
            data.runCloudCode = true;
            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: 'REQUEST_BOOK_SAVE',
              success: 'BOOK_SAVE_SUCCESS',
              failure: 'BOOK_SAVE_ERROR'
            }, data);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: 'REQUEST_BOOK_SAVE'
    });
  },

  updateBook: function (modelClass, book, newData) {
    var promise;

    newData.runCloudCode = true;
    promise = adapter.update(modelClass, book.objectId, newData);
    newData.objectId = book.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOK_UPDATE',
      success: 'BOOK_UPDATE_SUCCESS',
      failure: 'BOOK_UPDATE_ERROR'
    }, newData);
  },

  deleteBook: function (modelClass, book) {
    var promise;

    book.runCloudCode = true;
    promise = adapter.destroy(modelClass, book.objectId);

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOK_DESTROY',
      success: 'BOOK_DESTROY_SUCCESS',
      failure: 'BOOK_DESTROY_ERROR'
    }, book);
  },

  clearNotifications: function () {
    AppDispatcher.dispatch({
      type: 'CLEAR_NOTIFICATIONS'
    });
  }
};