var React = require('react');
var LogStore = require('../stores/LogStore');

module.exports = React.createClass({
  getInitialState: function () {
    return {
      log: {
        type: LogStore.getType(),
        message: LogStore.getMessage()
      }
    }
  },

  componentDidMount: function () {
    LogStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function () {
    LogStore.removeChangeListener(this._onChange);
  },

  _onChange: function () {
    this.isMounted() && this.setState({
      log: {
        type: LogStore.getType(),
        message: LogStore.getMessage()
      }
    });
  },

  render: function () {
    var className = this.state.log.type ? 'log bg-' + this.state.log.type : '';

    return (
      <div>{this.state.log.message ? <p className={className}>{this.state.log.message}</p> : ''}</div>
    );
  }
});